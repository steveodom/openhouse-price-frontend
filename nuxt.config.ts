// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    '@nuxt/content',
    '@nuxtjs/color-mode',
    '@unocss/nuxt',
    '@vueuse/nuxt',
    '@ant-design-vue/nuxt',
    '@pinia/nuxt',
    '@pinia-plugin-persistedstate/nuxt'
  ],
  colorMode: {
    classSuffix: '',
  },
  css: [
    'ant-design-vue/dist/reset.css',
    '~/assets/css/app.styl'
  ],
  vite: {
    resolve: {
      alias: {
        'ant-design-vue/dist': 'ant-design-vue/dist',
        'ant-design-vue/es': 'ant-design-vue/es',
        'ant-design-vue/lib': 'ant-design-vue/es',
        'ant-design-vue': 'ant-design-vue/es'
      },
    },
  },
  runtimeConfig: {
    airtableKey: process.env.AIRTABLE_API_KEY,
    discourseBaseUrl: ""
    // Keys within public, will be also exposed to the client-side
  }
})
