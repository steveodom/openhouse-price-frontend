// NOT USED. DELETE
import { CostContent, CostType } from "~/types/costs";

function CostContentMixin<T extends new (...args: any[]) => {}>(Base: T) {
  return class extends Base {
    calculateCost(squareFootage: number) {
      let cost: number = 0;
      const self = this as unknown as CostContent; // Type assertion
      switch (self.cost_type) {
        case CostType.FIXED:
          cost = self.cost_unit_value;
          break;
        case CostType.FIXED_PLUS:
          cost = (self?.cost_plus_base || 0) + self.cost_unit_value * squareFootage;
          break;
        case CostType.PSF:
          cost = self.cost_unit_value * squareFootage;
          break;
        case CostType.LINEAR_FOOT:
          cost = self.cost_unit_value * (self.linear_foot_value || 0);
          break;
        default:
          console.error('Invalid cost type');
          return 100;
      }

      return cost;
    }
  }
}

export class CostContentClass extends CostContentMixin(class {}) {
  constructor(public costContent: CostContent) {
    super();
    Object.assign(this, costContent);
  }
}
