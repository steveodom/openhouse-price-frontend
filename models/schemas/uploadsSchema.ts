import { CostContent } from '~/types/costs';
import costsSchema from './costSchema.json'
const costContentSchema = costsSchema.definitions.CostContent;

// to update the costContentSchema, run `npm run build-schema`
export interface SchemaType {
  type: string;
  properties: {
    Items: {
      type: string;
      items: CostContent;
    };
    squareFootage: {
      type: string;
    };
  };
  required: string[];
}

const schema: SchemaType = {
  type: 'object',
  properties: {
    Items: {
      type: 'array',
      items: costContentSchema
    },
    squareFootage: {
      oneOf: [
        { type: 'number' },
        { type: 'string' }
      ]
    }
  },
  required: ['Items', 'squareFootage'],
}

export default schema