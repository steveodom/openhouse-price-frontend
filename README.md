# openhouse-price-frontend
Display prices of common housebuilding materials like 2x4's, drywall, and zip panels.

### Color Scheme
https://www.nordtheme.com/

## Deploying
I had trouble doing automatic deployements with Cloudflare. To work around the issue, I used wrangler to manually deploy (`yarn run deploy`).

## Getting started


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

