---
title: 'Underground Trenching'
created: '2022-06-29'
updated: '2022-06-29'
category: 'electrical'
cost_type: linear_foot
cost_unit_value: 3
discourse_id: 15
---

Trenching needed if the electrical service will be run underground.