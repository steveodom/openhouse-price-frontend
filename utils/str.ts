const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD'
})

const formatterNoDecimal = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 0,
  maximumFractionDigits: 0
})

export function toDollars(val: string | number = 0, rounded = false): string {
  const num = ensureNumber(val)
  return rounded ? formatterNoDecimal.format(num) : formatter.format(num)
}

export function ensureNumber(str: string | number = 0): number {
  return typeof str === 'string' ? parseFloat(str) : str
}

export function capitalize(str: string | undefined) {
  if (!str) return ''
  return str.charAt(0).toUpperCase() + str.slice(1)
}
