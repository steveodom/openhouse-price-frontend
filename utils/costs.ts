import { toDollars } from '~/utils/str'
import { CostContent, CostType } from '~/types/costs'

export const CostTypeLabels: { [key in CostType]: string } = {
  [CostType.FIXED]: 'Fixed',
  [CostType.FIXED_PLUS]: 'Fixed Plus',
  [CostType.PSF]: 'Cost Per Sq Foot',
  [CostType.LINEAR_FOOT]: 'Cost Per Linear Foot'
}

export function calculateCosts(items: CostContent[], squareFootage: number): string {
  let total = 0
  items.forEach((item) => {
    total += calculateCost(item, squareFootage)
  })
  return toDollars(total)
}

export function calculateCost(costItem:CostContent, squareFootage:number) {
  let cost: number = 0;
  switch (costItem.cost_type) {
    case CostType.FIXED:
      cost = costItem.cost_unit_value;
      break;
    case CostType.FIXED_PLUS:
      cost = (costItem?.cost_plus_base || 0) + costItem.cost_unit_value * squareFootage;
      break;
    case CostType.PSF:
      cost = costItem.cost_unit_value * squareFootage;
      break;
    case CostType.LINEAR_FOOT:
      cost = costItem.cost_unit_value * (costItem.linear_foot_value || 0);
      break;
    default:
      console.error('Invalid cost type');
      return 100;
  }

  return cost;
}


export function formHeader(costItem:CostContent, squareFootage:number) {
  let header: string = "Total";
  switch (costItem.cost_type) {
    case CostType.PSF:
      header = `based on ${ squareFootage } square footage`;
      break;
    default:
      // console.error('Invalid cost type');
      return header;
  }

  return header;
}
