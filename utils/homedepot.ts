export interface HomeDepotProduct {
  details: string
  title: string
  id: string
}

export interface HomeDepotProducts {
  [key: string]: HomeDepotProduct
}

export const availableProducts: HomeDepotProducts = {
  'zip': {
    details: '4\'x8\'x7/16',
    title: 'Zip Panel',
    id: '202089190'
  },
  '2x4': {
    details: '2\'x4\'x8\' prime whitewood',
    title: 'common 2x4',
    id: '312528776'
  },
  'osb': {
    details: '4\'x8\'x7/16',
    title: 'OSB Sheathing',
    id: '202106230'
  },
  'drywall': {
    details: '4\'x8\'x1/2 Lite-Weight Gypsum Board',
    title: 'Drywall',
    id: '202830343'
  }
}
