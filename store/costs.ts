import { defineStore } from 'pinia'
import { CostContent } from '~/types/costs'
import { CategoryContent } from '~/types/category'
import useCostContent from '~/composables/useCostContent'
import { calculateCost, calculateCosts, formHeader } from '~/utils/costs'
interface CostAssumptionsState {
  squareFootage: number
  Items: CostContent[],
  Categories: CategoryContent[],
  fromStorage: boolean
}

export const useCostAssumptions = defineStore('costAssumptions', {
  state: (): CostAssumptionsState => ({
    Items: [],
    Categories: [],
    squareFootage: 1200,
    fromStorage: false
  }),
  actions: {
    async fetchItems() {
      if (this.Items.length === 0) {
        this.Items = await useCostContent()
      }
    },
    async fetchCategories() {
      if (this.Categories.length === 0) {
        this.Categories = await useCategoryContent()
      }
    },
    saveCopy() {
      const copy = JSON.stringify({
        Items: this.Items,
        squareFootage: this.squareFootage
      })
      const element = document.createElement('a')
      element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(copy))
      element.setAttribute('download', 'costs.json')
      element.style.display = 'none'
      document.body.appendChild(element)
      element.click()
      document.body.removeChild(element)
    },
    updateItems(newItems: CostContent[], newSquareFootage: number) {
      this.Items = newItems;
      this.squareFootage = newSquareFootage;
    }
  },
  getters: {
    getItems(state) {
      return state.Items
    },
    getGroupCategories: (state: CostAssumptionsState) => (group_slug: string): CategoryContent[] => {
      return state.Categories.filter((category: CategoryContent) => category.group === group_slug)
    },
    getCategoryItems: (state) => (category: string | undefined) => {
      return state.Items.filter((item) => item.category === category)
    },
    getSquareFootage(state) {
      return state.squareFootage
    },
    calculateCost: (state) => (item: CostContent) => {
      return calculateCost(item, state.squareFootage)
    },
    calculateGroupTotal: (state) => (group: string) => {
      const categories = state.Categories.filter((category: CategoryContent) => category.group === group).map((category: CategoryContent) => category.slug)
      const filtered = state.Items.filter((item) => categories.includes(item.category))
      return calculateCosts(filtered, state.squareFootage)
    },
    calculateCategoryTotal: (state) => (category: string) => {
      const filtered = state.Items.filter((item) => item.category === category)
      return calculateCosts(filtered, state.squareFootage)
    },
    calculateTotal: (state) => {
      return calculateCosts(state.Items, state.squareFootage)
    },
    formHeader: (state) => (item: CostContent) => {
      return formHeader(item, state.squareFootage)
    },
  },
  // uses the pinia-persist plugin to persist state to localStorage
  persist: true
})
