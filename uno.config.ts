import {
  defineConfig,
  presetAttributify,
  presetIcons,
  presetTypography,
  presetUno,
  transformerDirectives,
  transformerVariantGroup
} from 'unocss'

export default defineConfig({
  safelist: [
  ],
  shortcuts: [],
  presets: [
    presetUno(),
    presetAttributify(),
    presetIcons({
      scale: 1.2
    }),
    presetTypography(),
  ],
  theme: {
    // ...
    colors: {
      white: '#ffffff' // class="text-very-cool"
    }
  },
  transformers: [
    transformerDirectives(),
    transformerVariantGroup()
  ]
})
