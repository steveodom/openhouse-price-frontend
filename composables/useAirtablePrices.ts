import AirtableBase from '~/utils/airtable'
export interface PriceRow {
  Date: string
  date: string
  Price: number
  'Product ID': string
  'Product Name': string
}

export interface PriceRows extends Array<PriceRow> {}

async function getData(): Promise<PriceRows> {
  const records = await AirtableBase('Prices')
    .select({
      fields: ['Date', 'Price', 'Product ID', 'Product Name'],
      sort: [{ field: 'Date', direction: 'asc' }], // Sort by Date field in ascending order
    })
    .all()
  const rows: PriceRows = []
  records.forEach((res) => {
    const record = res._rawJson.fields as PriceRow
    rows.push(record)
  })
  return rows
}

export default async function useAirtable() {
  return getData()
}
