import { CategoryContent } from '@/types/category'

export default async function useCategoryContent(): Promise<CategoryContent[]> {
  const contentQuery = await queryContent('categories')
    .without('body')
    .sort({ sort_order: 1 })
    .find() as CategoryContent[]
  return contentQuery
}
