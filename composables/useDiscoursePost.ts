import { ref } from 'vue';
import appConfig from '~/config'

export interface PostType {
  title: string;
  content: string;
}

interface DiscourseResponse {
  title: string;
  post_stream: {
    posts: Array<{ post_number: number; cooked: string; }>;
  };
}

export default async function useDiscoursePost(topicId: number) {
  const post = ref<PostType | null>({ title: '', content: '' })
  console.info(topicId)
  const { data, error } = await useFetch(`${appConfig.discourseBaseUrl}/t/${topicId}.json`)
  if (error.value) {
    console.error(error.value);
    return null;
  }

  const { title, post_stream } = (data.value as DiscourseResponse)
  // The first post is the wiki post
  const wikiPost = post_stream.posts.find((post: any) => post.post_number === 1);
  return post.value = {
    title: title,
    content: wikiPost?.cooked || ''
  }
}
