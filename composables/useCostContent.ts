import { CostContent } from '@/types/costs'

export default async function useCostContent(): Promise<CostContent[]> {
  const contentQuery = await queryContent('costs')
    .without('body')
    .find() as CostContent[]
  return contentQuery
}
