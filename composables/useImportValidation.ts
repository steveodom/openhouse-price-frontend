// validation of upload
// import $RefParser from 'json-schema-ref-parser'
import schema from '~/models/schemas/uploadsSchema'
import Ajv from 'ajv'

// async function inlineRefs(schema: object) {
//   console.info(schema, 'schema', $RefParser, '$RefParser')
//   const dereferencedSchema = await $RefParser.dereference(schema);
//   console.info(dereferencedSchema, 'dereferencedSchema')
//   return dereferencedSchema;
// }

export default async function useImportValidation(): Promise<ReturnType<typeof ajv.compile>> {
  const ajv = new Ajv()
  return ajv.compile(schema)
}
