import AirtableBase from '~/utils/airtable'
export interface PriceRowFiltered {
  Date: string;
  date: string;
  Price: number;
  'Product ID': string;
  'Product Name': string;
}

export interface PriceRowsFiltered extends Array<PriceRowFiltered> {}

async function getData(productId: string = '202106230'): Promise<PriceRowsFiltered> {
  const records = await AirtableBase('Prices')
    .select({
      fields: ['Date', 'Price', 'Product ID', 'Product Name'],
      filterByFormula: `{Product ID} = '${productId}'`,
      sort: [{ field: 'Date', direction: 'asc' }], // Sort by Date field in ascending order
    })
    .all()
  const rows: PriceRowsFiltered = []
  records.forEach(function (res) {
    const record = res._rawJson.fields as PriceRowFiltered
    record.id = res._rawJson.ID
    rows.push(record)
  })
  return rows
}

export default async function useAirtable(productId: string) {
  return getData(productId as string)
}
