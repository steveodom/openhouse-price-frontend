interface ScrollHeight {
  y: number
}

export function maxHeightTable(): ScrollHeight {
  if (typeof window !== 'undefined') {
    const height = window.innerHeight
    return { y: height - 300 }
  }
  return { y: 0 }
}
