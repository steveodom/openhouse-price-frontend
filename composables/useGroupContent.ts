import { GroupContent } from '@/types/groups'

export default async function useGroupContent(): Promise<GroupContent[]> {
  const contentQuery = await queryContent('groups')
    .without('body')
    .sort({ sort_order: 1 })
    .find() as GroupContent[]
  return contentQuery
}
