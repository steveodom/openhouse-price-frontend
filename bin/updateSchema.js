// scripts/updateSchema.js
const fs = require('fs');
const path = require('path');

const schemaPath = path.join(__dirname, '../models/schemas/costSchema.json');
const schema = require(schemaPath);

const costCategory = schema.definitions.CostCategory;
const costType = schema.definitions.CostType;

schema.definitions.CostContent.properties.category = costCategory;
schema.definitions.CostContent.properties.cost_type = costType;
schema.definitions.CostContent.additionalProperties = true;

fs.writeFileSync(schemaPath, JSON.stringify(schema, null, 2));