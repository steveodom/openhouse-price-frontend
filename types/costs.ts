import { CategoryContent } from "./category";

export enum CostType {
  FIXED = 'fixed',
  FIXED_PLUS = 'fixed_plus',
  PSF = 'psf',
  LINEAR_FOOT = 'linear_foot'
}

export type CategorySlug = CategoryContent['slug'];
export interface CostContent {
  _id: string
  title: string
  created: Date | string
  updated: Date | string
  category: CategorySlug
  cost_type: CostType
  cost_unit_value: number
  linear_foot_value?: number
  cost_plus_base?: number
  note?: string
  discourse_id?: number
  edit_component: string
}
