// Groups are the first level of the hierarchy
// First is a group, then a category, then a cost

export interface GroupContent {
  _id: string
  title: string
  slug: string // the shortname
  discourse_id?: number
  sort_order?: number
}
