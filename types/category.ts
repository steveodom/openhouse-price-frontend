// Categories are the second level of the hierarchy
// First is a group, then a category, then a cost

export interface CategoryContent {
  _id: string
  title: string
  group: string // CostGroup
  slug: string // the shortname
  discourse_id?: number
  sort_order?: number
}
